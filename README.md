# <center>dhm-echarts</center>

echarts3图表封装，前台ajax通过自定义div属性实现传值，后台通过各种数据适配器自动组装达到图表封装，以实现echarts图表展示,也可以自定义适配器。

项目地址：https://git.oschina.net/duhongming/dhm-echarts

项目交流QQ群：551709145 验证码：码云

文档地址：http://files.hrhx.com.cn/display/dhm2echarts
