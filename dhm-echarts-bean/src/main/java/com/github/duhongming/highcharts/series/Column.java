package com.github.duhongming.highcharts.series;

import com.github.abel533.echarts.series.Series;
import com.github.duhongming.highcharts.data.DataLabels;

/**
 * 3D柱状图
 * @author dhm
 * plotOptions.column
 *
 */
public class Column extends Series<Column>{

	private static final long serialVersionUID = -6067088980988521083L;
	/**
	 * Depth of the columns in a 3D column chart. Requires highcharts-3d.js.
	 */
	private int depth;
	private String cursor;
	private DataLabels dataLabels;
	
	public DataLabels dataLabels() {
		if (this.dataLabels == null) {
            this.dataLabels = new DataLabels();
        }
		return dataLabels;
	}

	public int depth() {
		return depth;
	}

	public Column depth(int depth) {
		this.depth = depth;
		return this;
	}

	public String cursor() {
		return cursor;
	}

	public Column cursor(String cursor) {
		this.cursor = cursor;
		return this;
	}
	
}
