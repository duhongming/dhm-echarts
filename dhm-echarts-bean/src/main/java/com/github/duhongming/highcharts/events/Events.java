package com.github.duhongming.highcharts.events;

public class Events {
	
	private String click;
	private String mouseOut;
	private String mouseOver;
	private String remove;
	private String select;
	private String unselect;
	private String update;
	
	public String click() {
		return click;
	}
	public Events click(String click) {
		this.click = click;
		return this;
	}
	public String mouseOut() {
		return mouseOut;
	}
	public Events mouseOut(String mouseOut) {
		this.mouseOut = mouseOut;
		return this;
	}
	public String mouseOver() {
		return mouseOver;
	}
	public Events mouseOver(String mouseOver) {
		this.mouseOver = mouseOver;
		return this;
	}
	public String remove() {
		return remove;
	}
	public Events remove(String remove) {
		this.remove = remove;
		return this;
	}
	public String select() {
		return select;
	}
	public Events select(String select) {
		this.select = select;
		return this;
	}
	public String unselect() {
		return unselect;
	}
	public Events unselect(String unselect) {
		this.unselect = unselect;
		return this;
	}
	public String getUpdate() {
		return update;
	}
	public Events setUpdate(String update) {
		this.update = update;
		return this;
	}
	
}
