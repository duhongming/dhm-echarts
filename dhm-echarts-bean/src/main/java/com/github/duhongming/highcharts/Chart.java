package com.github.duhongming.highcharts;

import com.github.abel533.echarts.code.SeriesType;
import com.github.duhongming.highcharts.d3.Options3d;
import com.github.duhongming.highcharts.events.Events;
/**
 * 关于图表区和图形区的参数及一般图表通用参数。
 * @author dhm
 *
 */

public class Chart<T> {
	/**
	 * 图表描绘出后放到页面的某一具体位置
	 */
	 private String renderTo;
	
	private SeriesType type;
	
	private Boolean inverted;
	
	/**
	 * 3D图像设置项。3D效果需要引入highcharts-3d.js，下载或者在线路径为code.highcharts.com/highcharts-3d.js.
	 */
	private Options3d options3d;
	
	/**
	 * 边距是指图表的外边与图形区域之间的距离，数组分别代表上、右、下和左。要想单独设置可以用marginTop,marginRight,marginBotton 和 marginLeft.
	 */
	private int[] margin;
	
	private Events events;	
	
	public Events events() {
		if (this.events == null) {
            this.events = new Events();
        }
		return this.events;
	}
	
	public Options3d options3d() {
		 if (this.options3d == null) {
            this.options3d = new Options3d();
         }
	     return this.options3d;
	}	

    /**
     * 设置type值
     *
     * @param type
     */
	public Chart<T> type(SeriesType type) {
        this.type = type;
        return this;
    }
	public String getRenderTo() {
		return renderTo;
	}
	public void setRenderTo(String renderTo) {
		this.renderTo = renderTo;
	}
	public Boolean inverted() {
		return inverted;
	}
	public Chart inverted(Boolean inverted) {
		this.inverted = inverted;
		return this;
	}

	public int[] margin() {
		return margin;
	}
	public Chart margin(int[] margin) {
		this.margin = margin;
		return this;
	}
}
