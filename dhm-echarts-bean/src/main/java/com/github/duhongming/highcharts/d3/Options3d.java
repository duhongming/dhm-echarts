package com.github.duhongming.highcharts.d3;

/**
 * 3D图像设置项。3D效果需要引入highcharts-3d.js，下载或者在线路径为code.highcharts.com/highcharts-3d.js.
 * @author dhm
 *
 */

public class Options3d {
	/**
	 * 画图表是否启用3D函数，默认值为：false
	 */
	private Boolean enabled;
	
	/**
	 * 3D图旋转角度，此为α角，内旋角度默认是： 0.
	 */
	private Integer alpha;
	
	/**
	 * 3D图旋转角度，此为β角，外旋角度默认是： 0.
	 */
	private Integer beta;
	
	/**
	 * 图表的全深比，即为3D图X，Y轴的平面点固定，以图的Z轴原点为起始点上下旋转，
	 * 值越大往外旋转幅度越大，值越小往内旋转越大，depth的默认值为100
	 */
	private Integer depth;
	
	/**
	 * 它定义了观看者在图前看图的距离，它是非常重要的对于计算角度影响在柱图和散列图，此值不能用于3D的饼图，默认值为100默认是： 100.
	 */
	private Integer viewDistance;

	public Boolean enabled() {
		return enabled;
	}

	public Options3d enabled(Boolean enabled) {
		this.enabled = enabled;
		return this;
	}

	public Integer alpha() {
		return alpha;
	}

	public Options3d alpha(Integer alpha) {
		this.alpha = alpha;
		return this;
	}

	public Integer beta() {
		return beta;
	}

	public Options3d beta(Integer beta) {
		this.beta = beta;
		return this;
	}

	public Integer depth() {
		return depth;
	}

	public Options3d depth(Integer depth) {
		this.depth = depth;
		return this;
	}

	public Integer viewDistance() {
		return viewDistance;
	}

	public Options3d viewDistance(Integer viewDistance) {
		this.viewDistance = viewDistance;
		return this;
	}
	
	
}
