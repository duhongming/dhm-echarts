package com.github.duhongming.highcharts.data;

/**
 * 
 * @author dhm
 * The Data module provides a simplified interface for adding data to a chart from sources like CVS, HTML tables or grid views.
 *  See also the tutorial article on the Data module.
 *
 */

public class Data {
	
	/**
	 * The name of the point as shown in the legend, tooltip, dataLabel etc.
	 */
	private String name;
	
	/**
	 * The y value of the point.
	 */
	private Object y;
	private String url;
	/**
	 * Pie series only. Whether to display a slice offset from the center.
	 */
	private Boolean sliced;
	
	/**
	 * Whether to select the series initially. If showCheckbox is true, the checkbox next to the series name will be checked for a selected series.
	 */
	private Boolean selected; 
}
