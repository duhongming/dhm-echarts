package com.github.duhongming.highcharts.data;

public class DataLabels {
	
	private Boolean enabled;
	private String format;
	private Integer distance;
	private String style;
	
	public Boolean enabled() {
		return enabled;
	}
	public DataLabels enabled(Boolean enabled) {
		this.enabled = enabled;
		return this;
	}
	public String format() {
		return format;
	}
	public DataLabels format(String format) {
		this.format = format;
		return this;
	}
	public Integer distance() {
		return distance;
	}
	public DataLabels distance(Integer distance) {
		this.distance = distance;
		return this;
	}
	public String style() {
		return style;
	}
	public DataLabels style(String style) {
		this.style = style;
		return this;
	}
	
	

}
