package com.github.duhongming.highcharts.series;

import com.github.duhongming.highcharts.events.Events;


public class Point {
	private Events events;	
	
	public Events events() {
		if (this.events == null) {
            this.events = new Events();
        }
		return this.events;
	}
}
