package com.github.duhongming.highcharts.d3;

import com.github.abel533.echarts.series.Pie;
import com.github.duhongming.highcharts.series.Column;
import com.github.duhongming.highcharts.series.Series;
/**
 * The plotOptions is a wrapper object for config objects for each series type. 
 * The config objects for each series can also be overridden for each series item as given in the series array.
 * @author dhm
 *
 */
public class PlotOptions {
	
	private Column column;
	/**
     * 驱动图表生成的数据内容（详见series），数组中每一项代表一个系列的特殊选项及数据
     */
    private Series series;
    
    private Pie pie;
    
    public Pie pie() {
		if (this.pie == null) {
            this.pie = new Pie();
        }
		return pie;
	}
	/**
     * 驱动图表生成的数据内容（详见series），数组中每一项代表一个系列的特殊选项及数据
     */
    public Series series() {
        if (this.series == null) {
            this.series = new Series();
        }
        return series;
    }

	public Column column() {
		if (this.column == null) {
            this.column = new Column();
        }
		return column;
	}	
	
}
