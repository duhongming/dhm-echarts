package com.github.duhongming.highcharts.d3;

/**
 * 通过颜色线横贯在绘图区域上标记轴中的一个特定值
 * @author dhm
 *
 */

public class PlotLines {
	/**
	 * 区域划分线代表的值
	 */
	private Double value;
	
	/**
	 * 区域划分线的宽度或者厚度
	 */
	private Double width;
	
	/**
	 * 区域划分线的颜色
	 */
	private String color;
	
}
