package com.github.duhongming.vis;

public class ItemsDataSet {
	
	private String id;
	private String content;
	private String start;
	private String end;
	private String group;
	
	public ItemsDataSet() {
		super();
	}
	
	public ItemsDataSet(String id, String content, String start, String end) {
		super();
		this.id = id;
		this.content = content;
		this.start = start;
		this.end = end;
	}
	
	public ItemsDataSet(String id, String content, String start, String end, String group) {
		super();
		this.id = id;
		this.content = content;
		this.start = start;
		this.end = end;
		this.group = group;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}

}
