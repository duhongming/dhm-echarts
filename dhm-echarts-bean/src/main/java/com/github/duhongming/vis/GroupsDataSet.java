package com.github.duhongming.vis;

public class GroupsDataSet {
	
	private String id;
	private String content;
	private String[] nestedGroups;
	private Boolean visible;
	private Boolean showNested;
	
	public GroupsDataSet() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String[] getNestedGroups() {
		return nestedGroups;
	}

	public void setNestedGroups(String[] nestedGroups) {
		this.nestedGroups = nestedGroups;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public Boolean getShowNested() {
		return showNested;
	}

	public void setShowNested(Boolean showNested) {
		this.showNested = showNested;
	}
	
}
