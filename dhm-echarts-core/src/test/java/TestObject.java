import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestObject {

	public static void main(String[] args) {
//		List<Object[]> objList = EchartsDoubleNumLineAdapter.ME.convertListObjectToListObjectArray(getCalendarData());
//		System.out.println(new JsonUtil().toStrings(objList));
//		List<Object[]> objList1 = EchartsDoubleNumLineAdapter.ME.convertListObjectToListObjectArray(getCalendarDataTemp());
//		System.out.println(new JsonUtil().toStrings(objList1));
	}

	public static List<Map<String,Object>> getCalendarData(){
		int maxDaysOfMonth = 31;
		List<Map<String,Object>> list = new ArrayList<>();
		for(int i=1;i<=maxDaysOfMonth;i++){
			String dateDay="";
			if(i<10){
				dateDay = "0"+i;
			}else{
				dateDay = ""+i;
			}
			Map<String,Object> map = new HashMap<>(16);
			map.put("echartsDate", "2017-05-"+dateDay);
			map.put("工作", randomData()/100);
			map.put("睡觉", randomData()/100);
			map.put("娱乐", randomData()/100);
			list.add(map);
		}
		return list;
	}
	
	public static List<TestBean> getCalendarDataTemp(){
		int maxDaysOfMonth = 31;
		List<TestBean> list = new ArrayList<>();
		for(int i=1;i<=maxDaysOfMonth;i++){
			TestBean testBean = new TestBean( randomData()/100,randomData()/100);
			list.add(testBean);
		}
		return list;
	}
	
	private static double randomData() {
	    return Math.round(Math.random()*1000);
	}
}
