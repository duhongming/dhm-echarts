
public class TestBean {
	
	private Double x;
	private Double y;
	private Double z;
	
	public TestBean() {
		super();
	}
	
	public TestBean(Double x, Double y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	public Double getX() {
		return x;
	}
	public void setX(Double x) {
		this.x = x;
	}
	public Double getY() {
		return y;
	}
	public void setY(Double y) {
		this.y = y;
	}

	public Double getZ() {
		return z;
	}

	public void setZ(Double z) {
		this.z = z;
	}
	
}
