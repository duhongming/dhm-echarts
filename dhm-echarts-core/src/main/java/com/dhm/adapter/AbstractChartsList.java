package com.dhm.adapter;

import com.dhm.bean.EchartsConfig;
import com.github.abel533.echarts.json.GsonOption;

import java.util.List;
/**
 * @Author duhongming
 * @Email 19919902414@189.cn
 * @Date 2018/6/14 14:06
 */
public abstract class AbstractChartsList<T> implements ChartsList<T>{
    @Override
    public GsonOption adapter(EchartsConfig echartsConfig, 
                              GsonOption gsonOption, 
                              List<T> data) {
        return new GsonOption();
    }
}
