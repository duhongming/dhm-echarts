package com.dhm.adapter;

import com.dhm.bean.EchartsConfig;
import com.github.abel533.echarts.json.GsonOption;

import java.util.Map;

/**
 * charts Map数据适配器接口
 * @Author duhongming
 * @Email 19919902414@189.cn
 * @Date 2018/6/14 14:04
 */
public interface ChartsMap<K,V> extends Charts<Map<K,V>>{
    /**
     * 适合图表类型：
     * EchartsMapAdapter
     * EchartsPieAdapter
     * HighChartsPie3dAdapter
     * @param echartsConfig
     * @param gsonOption
     * @param data
     * @return
     */
    GsonOption adapter(EchartsConfig echartsConfig,
                       GsonOption gsonOption,
                       Map<K,V> data);
}
