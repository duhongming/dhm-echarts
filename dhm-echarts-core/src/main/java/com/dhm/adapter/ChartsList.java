package com.dhm.adapter;

import com.dhm.bean.EchartsConfig;
import com.github.abel533.echarts.json.GsonOption;

import java.util.List;

/**
 * charts List数据适配器接口
 * @Author duhongming
 * @Email 19919902414@189.cn
 * @Date 2018/6/14 14:04
 */
public interface ChartsList<T> extends Charts<List<T>>{
    /**
     * 适合图表类型：
     * EchartsLineAndBarAdapter
     * EchartsReverseBarAdapter
     * EchartsRadarAdapter
     * HighChartsBar3dAdapter
     * @param echartsConfig
     * @param gsonOption
     * @param data
     * @return
     */
    GsonOption adapter(EchartsConfig echartsConfig,
                       GsonOption gsonOption,
                       List<T> data);
}
