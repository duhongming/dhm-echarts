package com.dhm.adapter;

import com.dhm.bean.EchartsConfig;
import com.github.abel533.echarts.json.GsonOption;


/**
 * @Author duhongming
 * @Email 19919902414@189.cn
 * @Date 2018/6/14 14:06
 */
public abstract class AbstractChartsArray implements ChartsArray{
    @Override
    public GsonOption adapter(EchartsConfig echartsConfig,
                              GsonOption gsonOption,
                              Object[][] data) {
        return new GsonOption();
    }
}
