package com.dhm.adapter;

import com.dhm.bean.EchartsConfig;
import com.github.abel533.echarts.json.GsonOption;


/**
 * Charts图表接口
 * @Author duhongming
 * @Email 19919902414@189.cn
 * @Date 2018/6/14 14:01
 * @param <T>
 */
public interface Charts<T> {
    /**
     * 将前台配置EchartsConfig和后台数据T进行整合
     * 通过ajax输出给页面GsonOption的json数据格式
     * @param echartsConfig
     * @param gsonOption
     * @param data
     * @return
     */
    GsonOption adapter(EchartsConfig echartsConfig,
                       GsonOption gsonOption,
                       T data);
}
