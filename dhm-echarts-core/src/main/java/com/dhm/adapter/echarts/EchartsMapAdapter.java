package com.dhm.adapter.echarts;

import com.dhm.adapter.AbstractChartsMap;
import com.dhm.adapter.Charts;
import com.dhm.bean.EchartsConfig;
import com.github.abel533.echarts.code.SeriesType;
import com.github.abel533.echarts.data.PointData;
import com.github.abel533.echarts.json.GsonOption;

import java.util.Map;
/**
 * 
 * @author duhongming
 *
 */
public class EchartsMapAdapter extends AbstractChartsMap<String, Map<String, Double>> {
	
	public static final Charts ME = new EchartsMapAdapter();
	/**
	 * 适配器入口
	 * @param echartsConfig
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	public GsonOption adapter(EchartsConfig echartsConfig, GsonOption gsonOption,
                              Map<String, Map<String, Double>> data) {
		if(data.isEmpty()){
			throw new RuntimeException("Collection no data!");
		}
		gsonOption = legendAdapter(gsonOption,data);
		gsonOption = seriesAdapter(echartsConfig,gsonOption,data);
		return gsonOption;
	}

	/**
	 * legendAdapter
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	private GsonOption legendAdapter(GsonOption gsonOption,Map<String, Map<String, Double>> data){
		for (String key : data.keySet()) {
			//图例组件
			gsonOption.legend().data(key);
		}
		return gsonOption;
	}
	
	/**
	 * 数据处理
	 * @param echartsConfig
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	private GsonOption seriesAdapter(EchartsConfig echartsConfig,GsonOption gsonOption,Map<String, Map<String, Double>> data){
		Integer max=0;
		Integer min=0;
		for(Object legendData : gsonOption.legend().data()){
			
			com.github.abel533.echarts.series.Map echartMap = new com.github.abel533.echarts.series.Map();	
			echartMap = echartMap.type(SeriesType.map)
					   			 .name((String)legendData)
					   			 .mapType("china");
			echartMap.label().normal().show(true);
			echartMap.label().emphasis().show(true);
			Map<String,Double> map = data.get(legendData);
			for (String key : map.keySet()) {
				if(map.get(key) > max){
					max = map.get(key).intValue();
				}
				if(min > map.get(key)){
					min = map.get(key).intValue();
				}
				echartMap.data(new PointData(key,map.get(key)));
			}
			gsonOption.series(echartMap);
		}
		gsonOption.visualMap().get(0).min(min).max(max*gsonOption.legend().data().size());
		return gsonOption;
	}
}
