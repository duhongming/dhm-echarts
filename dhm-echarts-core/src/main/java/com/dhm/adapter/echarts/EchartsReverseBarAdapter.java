package com.dhm.adapter.echarts;

import com.dhm.adapter.AbstractChartsList;
import com.dhm.adapter.Charts;
import com.dhm.bean.EchartsConfig;
import com.github.abel533.echarts.axis.CategoryAxis;
import com.github.abel533.echarts.axis.ValueAxis;
import com.github.abel533.echarts.code.AxisType;
import com.github.abel533.echarts.code.Position;
import com.github.abel533.echarts.json.GsonOption;
import com.github.abel533.echarts.series.Bar;

import java.util.List;
import java.util.Map;
/**
 * 
 * @author duhongming
 *
 */
public class EchartsReverseBarAdapter extends AbstractChartsList<Map<String,Object>> {
	
	public static final Charts ME = new EchartsReverseBarAdapter();
	/**
	 * 反转bar的适配器入口
	 * @param echartsConfig
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	public GsonOption adapter(EchartsConfig echartsConfig,
			GsonOption gsonOption,
			List<Map<String,Object>> data){
		if(data.isEmpty()){
			throw new RuntimeException("Collection no data!");
		}
		gsonOption = legendAdapter(gsonOption,data);
		gsonOption = xAxisAdapter(echartsConfig,gsonOption,data);
		gsonOption = yAxisAdapter(echartsConfig,gsonOption,data);
		gsonOption = seriesAdapter(echartsConfig,gsonOption,data);
		return gsonOption;
	}
	
	/**
	 * legendAdapter
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	private GsonOption legendAdapter(GsonOption gsonOption,List<Map<String,Object>> data){
		//获取第一个List中的Map中的值
		for (String key : data.get(0).keySet()) {
			String[] k = key.split(":");
			//图例组件：Y轴数据类型
			if(k[0].startsWith("y")){
				gsonOption.legend().data(k[1]);
			}
		}
		return gsonOption;
	}
	
	/**
	 * x轴配置以及单位配置
	 * @param echartsConfig
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	private GsonOption xAxisAdapter(EchartsConfig echartsConfig,GsonOption gsonOption,List<Map<String,Object>> data){
		ValueAxis valueAxis = new ValueAxis();
		valueAxis.name(echartsConfig.getXaxisName());
		valueAxis.type(AxisType.value);
		gsonOption.xAxis(valueAxis);
		return gsonOption;
	}
	
	/**
	 * Y轴单位配置
	 * @param echartsConfig
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	private GsonOption yAxisAdapter(EchartsConfig echartsConfig,GsonOption gsonOption,List<Map<String,Object>> data){
		CategoryAxis categoryAxis = new CategoryAxis();
		categoryAxis.name(echartsConfig.getYaxisName());
		// Y轴数据封装并解析
		for (Map<String,Object> map : data) {
			//遍历Map的有关x:deX轴数据
			for (String key : map.keySet()) {
				if(key.startsWith("x:")){
					categoryAxis.type(AxisType.category).data(map.get(key));
				}
			}
		}
		gsonOption.yAxis(categoryAxis);
		return gsonOption;
	}
	
	/**
	 * Y轴数据处理
	 * @param echartsConfig
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	private GsonOption seriesAdapter(EchartsConfig echartsConfig,GsonOption gsonOption,List<Map<String,Object>> data){
		for(Object legendData : gsonOption.legend().data()){
			Bar line = new Bar();
			if(echartsConfig.getIsNeedStack()){				
				line.stack("总量").label().normal().show(true).position(Position.insideRight);
			}
			line = line.name((String)legendData);
			for (Map<String,Object> map : data) {
				//每个图表的Y轴数据匹配
				for (String key : map.keySet()) {
					if(key.endsWith((String)legendData)){
						line.data(map.get(key));
					}
				}
			}
			gsonOption.series(line);
		}
		return gsonOption;
	}
}
