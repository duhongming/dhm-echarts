package com.dhm.adapter.echarts;

import com.dhm.adapter.AbstractChartsMap;
import com.dhm.adapter.Charts;
import com.dhm.bean.EchartsConfig;
import com.github.abel533.echarts.code.RoseType;
import com.github.abel533.echarts.code.SeriesType;
import com.github.abel533.echarts.data.Data;
import com.github.abel533.echarts.json.GsonOption;
import com.github.abel533.echarts.series.Pie;

import java.util.Map;
/**
 * 
 * @author duhongming
 *
 */
public class EchartsPieAdapter extends AbstractChartsMap<String,Object> {
	
	public static final Charts ME = new EchartsPieAdapter();
	
	/**
	 * 适配器入口
	 * @param echartsConfig
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	public GsonOption adapter(
			EchartsConfig echartsConfig,
			GsonOption gsonOption,
			Map<String,Object> data){
		if(data.isEmpty()){
			throw new RuntimeException("Collection no data!");
		}
		gsonOption = legendAdapter(gsonOption, data);
		gsonOption = seriesAdapter(echartsConfig,gsonOption, data);
		return gsonOption;
	}
		
	/**
	 * legendAdapter
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	private GsonOption legendAdapter(
			GsonOption gsonOption,
			Map<String,Object> data){
		for (String key : data.keySet()) {
			gsonOption.legend().data(key);
		}
		return gsonOption;
	}
	/**
	 * radius轴数据处理
	 * @param echartsConfig
	 * @param gsonOption
	 * @param orientData
	 * @return
	 */
	private GsonOption seriesAdapter(
			EchartsConfig echartsConfig,
			GsonOption gsonOption,
			Map<String,Object> orientData){
			Pie pie = new Pie();
			//饼图数值显示
			//if(itemStyleShow){
			pie.itemStyle().normal().label()
											//.show(itemStyleShow)
											.formatter("{b} : {c}");
			//}

			pie.name(echartsConfig.getTitle()).type(echartsConfig.getSeriesType());

			if (echartsConfig.getSeriesType() == SeriesType.rose){
				pie.name(echartsConfig.getTitle()).type(SeriesType.pie);
				pie.roseType(RoseType.radius);
			}

			for (String title : orientData.keySet()) {
				Object value = orientData.get(title);
				pie.data(new Data().name(title).value(value));
			}
			gsonOption.series(pie);
		return gsonOption;
	}
}
