package com.dhm.echarts;

import com.dhm.adapter.AbstractChartsArray;
import com.dhm.adapter.Charts;
import com.dhm.bean.EchartsConfig;
import com.github.abel533.echarts.axis.ValueAxis;
import com.github.abel533.echarts.code.AxisType;
import com.github.abel533.echarts.code.LineType;
import com.github.abel533.echarts.code.Position;
import com.github.abel533.echarts.json.GsonOption;
import com.github.abel533.echarts.series.Scatter;

/**
 * 
 * @author duhongming
 *
 */
public class EchartsScatterAdapter extends AbstractChartsArray {
	
	public static final Charts ME = new EchartsScatterAdapter();
	
	/**
	 * scatter的适配器入口
	 * @param echartsConfig
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	public GsonOption adapter(EchartsConfig echartsConfig,
			GsonOption gsonOption,
			Object[][] data){
		gsonOption = xyAxisAdapter(gsonOption);
		gsonOption = seriesAdapter(echartsConfig,gsonOption,data);
		return gsonOption;
	}
	
	
	/**
	 * x轴配置和Y轴配置
	 * @param gsonOption
	 * @return
	 */
	private GsonOption xyAxisAdapter(GsonOption gsonOption){
		ValueAxis valueAxis = new ValueAxis();
		valueAxis.type(AxisType.value);
		valueAxis.splitLine().lineStyle().type(LineType.dashed);
		gsonOption.xAxis(valueAxis);
		gsonOption.yAxis(valueAxis);
		return gsonOption;
	}
	
	/**
	 * Y轴数据处理
	 * @param echartsConfig
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private GsonOption seriesAdapter(EchartsConfig echartsConfig,GsonOption gsonOption,Object[][] data){
		Scatter scatter = new Scatter();
		scatter.name("scatter")
			   .label().emphasis().show(true)
			   					  .position(Position.left)
			   					  .textStyle().color("blue").fontSize(16);
		for (int num = 0; num < data.length; num++) {
			scatter.data().add(data[num]);
		};
		gsonOption.series(scatter);
		return gsonOption;
	}
}
