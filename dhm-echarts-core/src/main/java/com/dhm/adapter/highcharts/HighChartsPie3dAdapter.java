package com.dhm.adapter.highcharts;

import com.dhm.adapter.AbstractChartsMap;
import com.dhm.adapter.Charts;
import com.dhm.bean.EchartsConfig;
import com.github.abel533.echarts.code.SeriesType;
import com.github.abel533.echarts.data.Data;
import com.github.abel533.echarts.json.GsonOption;
import com.github.abel533.echarts.series.Line;

import java.util.Map;
/**
 * 
 * @author duhongming
 *
 */
public class HighChartsPie3dAdapter extends AbstractChartsMap<String, Object> {
	
	public static final Charts ME = new HighChartsPie3dAdapter();

	public GsonOption adapter(EchartsConfig echartsConfig, GsonOption gsonOption, Map<String, Object> data) {
		gsonOption = seriesAdapter(gsonOption, data);
		return gsonOption;
	}

	private GsonOption seriesAdapter(GsonOption gsonOption, Map<String, Object> orientData) {
		Line line = new Line();
		for (String title : orientData.keySet()) {	
			line.data(new Data().name(title).y(orientData.get(title))).type(SeriesType.pie).name("占比：");				
		}
		gsonOption.series(line);
		return gsonOption;
	}

}
