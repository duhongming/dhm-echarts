package com.dhm.adapter.highcharts;

import com.dhm.adapter.AbstractChartsList;
import com.dhm.adapter.Charts;
import com.dhm.bean.EchartsConfig;
import com.github.abel533.echarts.axis.CategoryAxis;
import com.github.abel533.echarts.axis.ValueAxis;
import com.github.abel533.echarts.json.GsonOption;
import com.github.duhongming.highcharts.series.Column;

import java.util.List;
import java.util.Map;
/**
 * 
 * @author duhongming
 *
 */
public class HighChartsBar3dAdapter extends AbstractChartsList<Map<String,Object>> {
	
	public static final Charts ME = new HighChartsBar3dAdapter();
	
	/**
	 * 适配器入口
	 * @param echartsConfig
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	public  GsonOption adapter(EchartsConfig echartsConfig,
			GsonOption gsonOption,
			List<Map<String,Object>> data){
		gsonOption = xAxisAdapter(echartsConfig,gsonOption,data);
		gsonOption = yAxisAdapter(echartsConfig,gsonOption,data);
		gsonOption = seriesAdapter(echartsConfig,gsonOption,data);
		return gsonOption;
	}
	
	/**
	 * x轴配置以及单位配置
	 * @param echartsConfig
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	private GsonOption xAxisAdapter(EchartsConfig echartsConfig,GsonOption gsonOption,List<Map<String,Object>> data){
		//X轴单位和分类数据	
		CategoryAxis categoryAxis = new CategoryAxis();
		categoryAxis.name(echartsConfig.getXaxisName());
		// X轴数据封装并解析
		if(data != null && data.size() != 0){
			for (Map<String,Object> map : data) {
				//遍历Map的有关x:deX轴数据
				for (String key : map.keySet()) {
					if(key.startsWith("x:")){
						categoryAxis.categories(map.get(key));
					}
				}
			}
		}
		gsonOption.xAxis(categoryAxis);
		return gsonOption;
	}
		
	/**
	 * Y轴单位配置
	 * @param echartsConfig
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	private GsonOption yAxisAdapter(EchartsConfig echartsConfig,GsonOption gsonOption,List<Map<String,Object>> data){
		String[] unitNameArray = echartsConfig.getYaxisName().split(",");
		for (String s : unitNameArray) {
			ValueAxis valueAxis = new ValueAxis();
			//设置y轴标题
			valueAxis.name(echartsConfig.getYaxisName());
			gsonOption.yAxis(valueAxis.name(s));
		}
		return gsonOption;
	}
		
	/**
	 * Y轴数据处理
	 * @param echartsConfig
	 * @param gsonOption
	 * @param data
	 * @return
	 */
	private GsonOption seriesAdapter(EchartsConfig echartsConfig,GsonOption gsonOption,List<Map<String,Object>> data){
		if(data != null && data.size() != 0){
			//获取第一个List中的Map中的值
			for (String key : data.get(0).keySet()) {
				String[] k = key.split(":");
				//图例组件：Y轴数据类型
				if(k[0].startsWith("y")){
					gsonOption.legend().data(k[1]);
				}
			}
		}
				
		for(Object legendData : gsonOption.legend().data()){
			Column column = new Column();
			//echarts-type图表类型
			if(data != null && data.size() != 0){
				for (Map<String,Object> map : data) {
					//每个图表的Y轴数据匹配
					for (String key : map.keySet()) {
						if(key.endsWith((String)legendData)){
							column.name((String)legendData).data(map.get(key));
						}
					}
				}
			}
			gsonOption.series(column);
		}
		return gsonOption;
	}
}
