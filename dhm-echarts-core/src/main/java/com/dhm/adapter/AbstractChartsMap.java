package com.dhm.adapter;


import com.dhm.bean.EchartsConfig;
import com.github.abel533.echarts.json.GsonOption;

import java.util.Map;
/**
 * @Author duhongming
 * @Email 19919902414@189.cn
 * @Date 2018/6/14 14:06
 */
public abstract class AbstractChartsMap<K,V> implements ChartsMap<K,V> {
    @Override
    public GsonOption adapter(EchartsConfig echartsConfig,
                              GsonOption gsonOption,
                              Map<K, V> data) {
        return new GsonOption();
    }
}
