package com.dhm.adapter;

import com.dhm.bean.EchartsConfig;
import com.github.abel533.echarts.json.GsonOption;

/**
 * charts 数组数据适配器接口
 * @Author duhongming
 * @Email 19919902414@189.cn
 * @Date 2018/6/14 14:02
 */
public interface ChartsArray extends Charts<Object[][]>{
    /**
     * 适合图表类型：
     * EchartsScatterAdapter
     * @param echartsConfig
     * @param gsonOption
     * @param data
     * @return
     */
    GsonOption adapter(EchartsConfig echartsConfig,
                       GsonOption gsonOption,
                       Object[][] data);
}
