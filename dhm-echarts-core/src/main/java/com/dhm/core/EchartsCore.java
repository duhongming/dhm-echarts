package com.dhm.core;

import com.dhm.adapter.echarts.*;
import com.dhm.bean.EchartsConfig;
import com.github.abel533.echarts.json.GsonOption;

import java.util.List;
import java.util.Map;

/**
 * 单例模式
 * @author duhongming
 *
 */
public class EchartsCore{

	private static EchartsCore instance;

	private EchartsCore(){
		super();
	}

	private static synchronized void syncInit(){
		if(instance == null){
			instance = new EchartsCore();
		}
	}

	public static EchartsCore getInstance(){
		if(instance == null){
			syncInit();
		}
        return instance;
    }

	public GsonOption getBaseGsonOption(EchartsConfig echartsConfig){
		EchartsBaseCore echartsBaseCore= new EchartsBaseCore();
		//1.获取option对象
		GsonOption gsonOption = echartsBaseCore.getBaseTitle(echartsConfig);
		//2.提示信息
		echartsBaseCore.getBaseTooltip(echartsConfig);
		//3.工具栏处理
		echartsBaseCore.getBarAndLineToolBox(echartsConfig);
		//4.数据域缩放处理
		echartsBaseCore.getDataZoom(echartsConfig);
		//5.视觉映射组件
		echartsBaseCore.getVisualMap(echartsConfig);
		return gsonOption;
	}
	
	/**
	 * 柱状图和折线图适配器
	 * @param echartsConfig
	 * @param data
	 * @return GsonOption
	 */
	public GsonOption adapterLineAndBar(EchartsConfig echartsConfig,List<Map<String,Object>> data){		
		return EchartsLineAndBarAdapter.ME.adapter(echartsConfig,this.getBaseGsonOption(echartsConfig),data);
	}
	
	/**
	 * 饼状图适配器
	 * @param echartsConfig
	 * @param data
	 * @return GsonOption
	 */
	public GsonOption adapterPie(EchartsConfig echartsConfig,Map<String,Object> data){		
		return EchartsPieAdapter.ME.adapter(echartsConfig,this.getBaseGsonOption(echartsConfig),data);
	}
	
	/**
	 * 散点图适配器
	 * @param echartsConfig
	 * @param data
	 * @return GsonOption
	 */
	public GsonOption adapterScatter(EchartsConfig echartsConfig,Object[][] data){		
		return com.dhm.echarts.EchartsScatterAdapter.ME.adapter(echartsConfig,this.getBaseGsonOption(echartsConfig),data);
	}
	
	/**
	 * 双数值折线图适配器<p>
	 * 时间数值折线图适配器
	 * @param echartsConfig
	 * @param data
	 * @return
	 */
	public <T> GsonOption adapterDoubleNumLine(EchartsConfig echartsConfig,Map<String,List<T>> data){		
		return EchartsDoubleNumLineAdapter.ME.adapter(echartsConfig,this.getBaseGsonOption(echartsConfig),data);
	}
	
	/**
	 * 反转柱状图适配器
	 * @param echartsConfig
	 * @param data
	 * @return GsonOption
	 */
	public GsonOption adapterReverseBar(EchartsConfig echartsConfig,List<Map<String,Object>> data){		
		return EchartsReverseBarAdapter.ME.adapter(echartsConfig,this.getBaseGsonOption(echartsConfig),data);
	}
	
	/**
	 * 雷达图适配器
	 * @param echartsConfig
	 * @param data
	 * @return GsonOption
	 */
	public GsonOption adapterRadar(EchartsConfig echartsConfig,List<Map<String,Object>> data){		
		return EchartsRadarAdapter.ME.adapter(echartsConfig,this.getBaseGsonOption(echartsConfig),data);
	}
	
	/**
	 * 地图适配器
	 * @param echartsConfig
	 * @param data
	 * @return GsonOption
	 */
	public GsonOption adapterMap(EchartsConfig echartsConfig,Map<String, Map<String, Double>> data) {
		return EchartsMapAdapter.ME.adapter(echartsConfig,this.getBaseGsonOption(echartsConfig),data);
	}
	
}
