package com.dhm.core;

import com.dhm.adapter.highcharts.HighChartsBar3dAdapter;
import com.dhm.adapter.highcharts.HighChartsPie3dAdapter;
import com.dhm.bean.EchartsConfig;
import com.github.abel533.echarts.json.GsonOption;

import java.util.List;
import java.util.Map;
/**
 * 
 * @author duhongming
 *
 */
public class HighChartsCore {

	private static HighChartsCore instance;
	private static final String BAR3D = "bar3d";
	private static final String PIE3D = "pie3d";

	private HighChartsCore(){
		super();
	}

	private static synchronized void syncInit(){
		if(instance == null){
			instance = new HighChartsCore();
		}
	}
	
	public static HighChartsCore getInstance(){
		if(instance == null){
			syncInit();
		}
		return instance;
    }
	
	public GsonOption getBaseGsonOption(EchartsConfig echartsConfig){
//		//1.获取option对象
//		GsonOption gsonOption = new GsonOption();
//		//设置标题、设置子标题
//		gsonOption.title(echartsConfig.getTitle(),echartsConfig.getSubtitle());
//		if(BAR3D.equals(echartsConfig.getType())){
//			//设置图表属性
//			gsonOption.chart()
//					  .inverted(false)
//					  .type(SeriesType.column)
//					  .margin(new int[]{75,75,75,75});
//			//设置图表3D属性
//			gsonOption.chart().options3d()
//							  .enabled(true)
//							  .alpha(10)
//							  .beta(25)
//							  .depth(70);
//			//图表标签显示格式
//			gsonOption.plotOptions().column()
//									.depth(25)
//									.cursor("pointer")
//									.dataLabels().enabled(true).format("{point.y:.2f}");
////			3D柱状图链接配置
////			TODO
////			gsonOption.plotOptions().series().point().events().click(
////					"function () {" +
////						" var para = 'Category=' + window.encodeURI(window.encodeURI(this.category)) + '&value=' + this.y + '&SeriesName=' + window.encodeURI(window.encodeURI(this.series.name)); " +
////						" window.open('http://www.baidu.com/'+para); "+
////					"}");
//
//		}else if(PIE3D.equals(echartsConfig.getType())){
//			//设置3D状态
//			gsonOption.chart().options3d().enabled(true).alpha(45).beta(0);
//			gsonOption.chart().margin(new int[]{25,25,25,25})
//						  .type(SeriesType.pie);
//			//工具显示框
//			gsonOption.tooltip().pointFormat("{series.name}: <b>{point.percentage:.2f}%</b>");
//			//3D饼状图基本配置
//			gsonOption.plotOptions().pie().allowPointSelect(true)
//									  .cursor("pointer")
//									  .depth(35)
//									  .dataLabels().enabled(true)
//									  			   //: {point.percentage:.2f}%
//									               .format("<b>{point.name}</b>")
//									               //.distance(-25)
//									               .style("fontWeight: 'bold',color: 'white',textShadow: '0px 1px 2px black'");
////			TODO
////			 gsonOption.plotOptions().series().point().events().click(
//// 						"function () {window.open(this.options.url);}
//// 						");
//		}
		
//		return gsonOption;
return null;
	}
	public GsonOption adapterBar3D(EchartsConfig echartsConfig,List<Map<String,Object>> data) {
		return HighChartsBar3dAdapter.ME.adapter(echartsConfig,this.getBaseGsonOption(echartsConfig),data);
	}
	public GsonOption adapterPie3D(EchartsConfig echartsConfig,Map<String, Object> data) {
		return HighChartsPie3dAdapter.ME.adapter(echartsConfig,this.getBaseGsonOption(echartsConfig),data);
	}
}
