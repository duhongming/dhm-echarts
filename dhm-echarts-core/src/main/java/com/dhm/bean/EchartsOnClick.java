package com.dhm.bean;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
/**
 * 
 * @author duhongming
 *
 */
public class EchartsOnClick{
	
	private String id;
	private String seriesIndex;
	private String seriesName;
	private String name;
	private String provinceCode;
	private Double value;
	
	private String x;
	private String y;
	
	public EchartsOnClick() {
		super();
	}
	
	public EchartsOnClick(String id, String seriesName, String name, Double value) {
		super();
		this.id = id;
		this.seriesName = seriesName;
		this.name = name;
		this.value = value;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	public String getSeriesIndex() {
		return seriesIndex;
	}

	public void setSeriesIndex(String seriesIndex) {
		this.seriesIndex = seriesIndex;
	}

	public String getSeriesName() {
		try {
			if(seriesName!=null){
				return URLDecoder.decode(seriesName, "UTF-8");
			}else{
				return "";
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return "";
		}
	}
	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}
	public String getName() {
		try {
			if(seriesName!=null){
				return URLDecoder.decode(name, "UTF-8");
			}else{
				return "";
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return "";
		}
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getProvinceCode() {
		switch(getName()){
		
			case "河北" : return "hebei";
			case "山西" : return "shanxi";
			case "内蒙古" : return "neimenggu";
			case "辽宁" : return "liaoning";
			case "吉林" : return "jilin";
			
			case "黑龙江" : return "heilongjiang";
			case "江苏" : return "jiangsu";
			case "浙江" : return "zhejiang";
			case "安徽" : return "anhui";
			case "福建" : return "fujian";
			
			case "江西" : return "jiangxi";
			case "山东" : return "shandong";
			case "河南" : return "henan";
			case "湖北" : return "hubei";
			case "湖南" : return "hunan";
			
			case "广东" : return "guangdong";
			case "广西" : return "guangxi";
			case "海南" : return "hainan";
			case "四川" : return "sichuan";
			case "贵州" : return "guizhou";
			
			case "云南" : return "yunnan";
			case "西藏" : return "xizang";
			case "陕西" : return "shanxi";
			case "甘肃" : return "gansu";
			case "青海" : return "qinghai";
			
			case "宁夏" : return "ningxia";
			case "新疆" : return "xinjiang";
			case "北京" : return "beijing";
			case "天津" : return "tianjin";
			case "上海" : return "shanghai";
			
			case "重庆" : return "chongqing";
			case "香港" : return "xianggang";
			case "澳门" : return "aomen";
			case "台湾" : return "taiwan";
			
			default : return provinceCode;
		}
		
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

	public String getY() {
		return y;
	}

	public void setY(String y) {
		this.y = y;
	}

}
