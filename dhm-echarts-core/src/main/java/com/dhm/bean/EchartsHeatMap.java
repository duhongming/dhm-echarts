package com.dhm.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * 
 * @author duhongming
 *
 */
public class EchartsHeatMap {
	
	private String[] x;
	private String[] y;
	private Integer[][] data;
	private Integer min=0;
	private Integer max=0;
	
	public EchartsHeatMap() {
		super();
	}
	
	public EchartsHeatMap(List<Map<String,Object>> list) {
		//x或者y的轴进行字段排序
		Map<String,Integer> xMap = getOrderMap("x",list);
		Map<String,Integer> yMap = getOrderMap("y",list);
		
		//将排好顺序的Map转成Array
		x = new String[xMap.keySet().size()];
		y = new String[yMap.keySet().size()];
		for(String xKey:xMap.keySet()){
			x[xMap.get(xKey)]=xKey;
		}
		for(String yKey:yMap.keySet()){
			y[yMap.get(yKey)]=yKey;
		}
		
		//将x(X轴数据)、y(Y轴数据)、z(值)封装到一个二维数组中
		//顺便将值的最小和最大封装好了，visualMap需要
		data = new Integer[list.size()][3];
		for(int i = 0;i < list.size();i++){
			Map<String,Object> map = list.get(i);
			data[i][0] = xMap.get(map.get("x"));
			data[i][1] = yMap.get(map.get("y"));
			data[i][2] = Integer.parseInt(map.get("z").toString());
			min = Math.min(min, data[i][2]);
			max = Math.max(max, data[i][2]);
		}
	}
	
	private Map<String,Integer> getOrderMap(String keyColums,List<Map<String,Object>> list){
		
		Map<String,Integer> xMap = new HashMap<String,Integer>(16);
		Map<String,Integer> tempMap = new HashMap<String,Integer>(16);
		
		//先把X轴或者Y轴数据存入xMap中
		for(Map<String,Object> map:list){
			xMap.put(map.get(keyColums).toString(),-1);
		}
		//Map中的key排序算法0、1、2、3....
		Integer i=0;
		for(Map<String,Object> map:list){
			xMap.putAll(tempMap);
			//遍历所有的xMap
			for(String xKey:xMap.keySet()){
				//如果key相同并且xKey还没有进行排序，所以需要对改字段进行排序
				if(xKey.equals(map.get(keyColums).toString())&&xMap.get(xKey)==-1){
					tempMap.put(xKey, i);
					i++;
				}
			}
		}
		return tempMap;
	}

	public String[] getX() {
		return x;
	}

	public void setX(String[] x) {
		this.x = x;
	}

	public String[] getY() {
		return y;
	}

	public void setY(String[] y) {
		this.y = y;
	}

	public Integer[][] getData() {
		return data;
	}

	public void setData(Integer[][] data) {
		this.data = data;
	}

	public Integer getMin() {
		return min;
	}

	public void setMin(Integer min) {
		this.min = min;
	}

	public Integer getMax() {
		return max;
	}

	public void setMax(Integer max) {
		this.max = max;
	}
}
