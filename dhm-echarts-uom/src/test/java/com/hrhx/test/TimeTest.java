package com.hrhx.test;

import org.junit.Test;

import com.hrhx.bean.uom.impl.Time;

public class TimeTest {
	@Test
	public void test() throws Exception {
		System.out.println(Time.ME.unitOfMeasureString(3600,Time.MINUTE,Time.HOUR,Time.ROUNDING));
		System.out.println(Time.ME.unitOfMeasureString(3600,Time.SECOND,Time.HOUR,Time.ROUNDING));
		
		System.out.println(Time.ME.unitOfMeasureString(3600,Time.HOUR,Time.MINUTE,Time.ROUNDING));
		
		System.out.println(Time.ME.unitOfMeasureString(1,Time.DAY,Time.SECOND,Time.ROUNDING));
		System.out.println(Time.ME.unitOfMeasureString(1,Time.WEEK,Time.SECOND,Time.ROUNDING));
		System.out.println(Time.ME.unitOfMeasureString(604800,Time.SECOND,Time.WEEK,Time.ROUNDING));
		
		System.out.println(Time.ME.unitOfMeasureString(1,Time.MONTH,Time.DAY,Time.ROUNDING2));
		System.out.println(Time.ME.unitOfMeasureString(1,Time.YEAR,Time.DAY,Time.ROUNDING));
		
	}
}
