package com.hrhx.test;

import org.junit.Test;

import com.hrhx.bean.uom.impl.Power;

public class PowerTest {
	@Test
	public void test() throws Exception {
		System.out.println(Power.ME.unitOfMeasureString(3600,Power.WATT,Power.KILOWATT,Power.ROUNDING));
		System.out.println(Power.ME.unitOfMeasureString(3600,Power.WATT,Power.MEGAWATT,Power.ROUNDING4));
		System.out.println(Power.ME.unitOfMeasureString(3600000,Power.WATT,Power.GIGAWATT,Power.ROUNDING4));
		
		System.out.println(Power.ME.unitOfMeasureString(1,Power.GIGAWATT,Power.WATT,Power.ROUNDING));
	}
}
