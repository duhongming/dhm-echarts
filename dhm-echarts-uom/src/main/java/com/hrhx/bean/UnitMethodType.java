package com.hrhx.bean;
/**
 * 单位转化公式Bean
 * @author duhongming
 *
 */
public class UnitMethodType {
	/**
	 * 因子数
	 */
	private Number factor;
	/**
	 * 偏移量
	 */
	private Number offset;
	/**
	 * 公式
	 */
	private String formula;
	
	public UnitMethodType() {
		super();
	}

	public UnitMethodType(Double factor, Double offset, String formula) {
		super();
		this.factor = factor;
		this.offset = offset;
		this.formula = formula;
	}

	public Number getFactor() {
		return factor;
	}

	public void setFactor(Number factor) {
		this.factor = factor;
	}

	public Number getOffset() {
		return offset;
	}

	public void setOffset(Number offset) {
		this.offset = offset;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}
}
