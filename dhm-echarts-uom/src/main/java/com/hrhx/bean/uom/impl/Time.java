package com.hrhx.bean.uom.impl;

import java.util.Map;

import com.hrhx.bean.UnitOfMeasure;
import com.hrhx.bean.uom.UOM;
import com.hrhx.util.XmlUtil;
/**
 * 时间单位
 * @author duhongming
 *
 */
public class Time extends UOM{
	
	public static final Time ME = new Time();
	
	/**
	 * 时间转换：秒
	 */
	public static final String SECOND = "second";
	/**
	 * 时间转换：分钟
	 */
	public static final String MINUTE = "minute";
	/**
	 * 时间转换：小时
	 */
	public static final String HOUR = "hour";
	/**
	 * 时间转换：天
	 */
	public static final String DAY = "day";
	/**
	 * 时间转换：周
	 */
	public static final String WEEK = "week";
	/**
	 * 时间转换：月
	 */
	public static final String MONTH = "month";
	/**
	 * 时间转换：年
	 */
	public static final String YEAR = "year";
	
	@SuppressWarnings("unchecked")
	public Time() {
		super();
		String templateFileName = System.getProperty("user.dir")+"/Time.xml";
		try {
			Object obj = XmlUtil.getInstance().fromXMLFile(templateFileName);
			unitOfMeasureMap.putAll((Map<String,UnitOfMeasure>)obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
