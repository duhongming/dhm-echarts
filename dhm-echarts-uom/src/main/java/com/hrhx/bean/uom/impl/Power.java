package com.hrhx.bean.uom.impl;

import java.util.Map;

import com.hrhx.bean.UnitOfMeasure;
import com.hrhx.bean.uom.UOM;
import com.hrhx.util.XmlUtil;

/**
 * 功率单位
 * @author duhongming
 *
 */
public class Power extends UOM{
	
	public static final Power ME = new Power();
	
	public static final String WATT = "watt";
	public static final String KILOWATT = "kilowatt";
	public static final String MEGAWATT = "megawatt";
	public static final String GIGAWATT = "gigawatt";
	
	@SuppressWarnings("unchecked")
	public Power() {
		super();
		String templateFileName = System.getProperty("user.dir")+"/Power.xml";
		try {
			Object obj = XmlUtil.getInstance().fromXMLFile(templateFileName);
			unitOfMeasureMap.putAll((Map<String,UnitOfMeasure>)obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
