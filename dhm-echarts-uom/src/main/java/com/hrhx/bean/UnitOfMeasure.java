package com.hrhx.bean;
/**
 * 单位转化Bean
 * @author duhongming
 *
 */
public class UnitOfMeasure {
	/**
	 * 单位名称
	 */
	private String name;
	/**
	 * 单位符号
	 */
	private String abbreviation;
	/**
	 * 单位描述
	 */
	private String description;
	/**
	 * 单位规范
	 */
	private String canonical;
	/**
	 * 单位引用
	 */
	private String reference;
	/**
	 * 单位方法
	 */
	private UnitMethodType unitMethodType;
	
	public UnitOfMeasure() {
		super();
	}
	
	public UnitOfMeasure(String name, String abbreviation, String description, String canonical, String reference,
			UnitMethodType unitMethodType) {
		super();
		this.name = name;
		this.abbreviation = abbreviation;
		this.description = description;
		this.canonical = canonical;
		this.reference = reference;
		this.unitMethodType = unitMethodType;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCanonical() {
		return canonical;
	}
	public void setCanonical(String canonical) {
		this.canonical = canonical;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public UnitMethodType getUnitMethodType() {
		return unitMethodType;
	}
	public void setUnitMethodType(UnitMethodType unitMethodType) {
		this.unitMethodType = unitMethodType;
	}
}
