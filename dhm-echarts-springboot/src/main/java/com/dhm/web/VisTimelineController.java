package com.dhm.web;

import com.alibaba.fastjson.JSON;
import com.github.duhongming.vis.ItemsDataSet;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author duhongming
 *
 */
@Controller
@RequestMapping("/vis")
public class VisTimelineController {

	@GetMapping(value = "timeline")
	public String visDemo(ModelMap modelMap){

		List<ItemsDataSet> dataSetList = new ArrayList<ItemsDataSet>();
		ItemsDataSet dataSet = new ItemsDataSet("1", "2014-07-07<BR/>2014-10-07<BR/>实习期：1500", "2014-07-07", "2014-10-07");
		dataSetList.add(dataSet);
		dataSet = new ItemsDataSet("2", "2014-10-08<BR/>2015-02-08<BR/>转正期：2500", "2014-10-08", "2015-02-08");
		dataSetList.add(dataSet);
		dataSet = new ItemsDataSet("3", "2015-02-09<BR/>2015-05-09<BR/>加薪到：3000", "2015-02-09", "2015-05-09");
		dataSetList.add(dataSet);
		dataSet = new ItemsDataSet("4", "2015-05-10<BR/>2016-02-28<BR/>加薪到：3750", "2015-05-10", "2016-02-28");
		dataSetList.add(dataSet);
		dataSet = new ItemsDataSet("5", "2016-02-28<BR/>2017-02-28<BR/>加薪到：4250", "2016-02-28", "2017-02-28");
		dataSetList.add(dataSet);
		dataSet = new ItemsDataSet("6", "2017-02-28<BR/>2017-09-20<BR/>加薪到：5100", "2017-02-28", "2017-09-20");
		dataSetList.add(dataSet);
		
		modelMap.put("dataSetList", JSON.toJSONString(dataSetList));

		return "Timeline";
		
	}
}
