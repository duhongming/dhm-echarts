package com.dhm.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 
 * @author duhongming
 *
 */
@Controller
@RequestMapping
public class IndexController {

	public String index(){
		return "redirect:index.html";
	}

}
