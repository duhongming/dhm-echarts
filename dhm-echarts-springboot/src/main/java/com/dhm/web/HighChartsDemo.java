package com.dhm.web;

import com.dhm.bean.EchartsConfig;
import com.dhm.core.HighChartsCore;
import com.dhm.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 
 * @author duhongming
 *
 */
@Controller
@RequestMapping("/highcharts")
public class HighChartsDemo {
	
	@Autowired
	private DataService dataService;
	
	/**
	 * 演示页面
	 * @return
	 */
	@GetMapping(value = "demo")
	public String echartsDemo(String theme,ModelMap modelMap){
		String themeUrl = "/highcharts/code/themes/"+theme+".js";
		modelMap.put("theme", theme);
		modelMap.put("themeUrl", themeUrl);
		return "/Highcharts";
	}
	
	/**
	 * 3D条形图
	 * @param echartsConfig
	 * @return
	 */
	@PostMapping(value = "bar3d",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody String bar3d(@RequestBody EchartsConfig echartsConfig){
		return HighChartsCore.getInstance().adapterBar3D(echartsConfig, dataService.getWeatherData()).toString();
	}
	
	/**
	 * 3D饼状图
	 * @param echartsConfig
	 * @return
	 */
	@PostMapping(value = "pie3d",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody String pie3d(@RequestBody EchartsConfig echartsConfig){
		return HighChartsCore.getInstance().adapterPie3D(echartsConfig, dataService.getPieData()).toString();
	}
	
}
