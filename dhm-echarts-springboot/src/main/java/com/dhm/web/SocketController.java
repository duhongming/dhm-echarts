package com.dhm.web;

import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * 
 * @author duhongming
 *
 */
@Controller
public class SocketController {
	
    @SendTo("/topic/line/data")
    public Object lineData(Object message) throws Exception {
		return message;
    }
    
    @SendTo("/topic/gauge/data")
    public Object gaugeData(Object message) throws Exception {
		return message;
    }

}
