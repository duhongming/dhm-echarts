package com.dhm.web;

import com.dhm.bean.EchartsConfig;
import com.dhm.bean.EchartsHeatMap;
import com.dhm.bean.EchartsOnClick;
import com.dhm.core.EchartsCore;
import com.dhm.service.DataService;
import com.github.abel533.echarts.code.AxisType;
import com.github.abel533.echarts.code.Orient;
import com.github.abel533.echarts.code.SeriesType;
import com.github.abel533.echarts.code.X;
import com.github.abel533.echarts.json.GsonOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 *
 * @author duhongming
 *
 */
@Controller
@RequestMapping("/echarts")
public class EchartsDemo {

	@Autowired
	private DataService dataService;

	/**
	 * 获取参数到前台显示
	 * 也可以通过制定控制器去请求其他图表的URL
	 * @param echartsOnClick
	 * @return
	 */
	@GetMapping(value = "onclick")
	public String onclick(EchartsOnClick echartsOnClick, ModelMap modelMap){
		modelMap.put("echartsOnClick",echartsOnClick);
		return "/common/onclick";
	}
	
	/**
	 * 折线图、条形图
	 * @param echartsConfig
	 * @return
	 */
	@PostMapping(value = "getWeatherData",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody String line(@RequestBody EchartsConfig echartsConfig){
		GsonOption gsonOption = EchartsCore.getInstance().adapterLineAndBar(echartsConfig,dataService.getWeatherData());
//		gsonOption.xAxis().get(0).axisLabel().interval(0).rotate(45);
		return gsonOption.toString();
	}
	
	/**
	 * 折线图条形图混合图
	 * @param echartsConfig
	 * @return
	 */
	@PostMapping(value = "barAndLine",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody String barAndLine(@RequestBody EchartsConfig echartsConfig){
		GsonOption gsonOption = EchartsCore.getInstance().adapterLineAndBar(echartsConfig,dataService.getWeatherData());
		//指定每条曲线的类型
		gsonOption.series().get(0).type(SeriesType.line);
		gsonOption.series().get(1).type(SeriesType.bar);
		return gsonOption.toString();
	}
	
	/**
	 * 饼状图
	 * @param echartsConfig
	 * @return
	 */
	@PostMapping(value = "pie",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody String pie(@RequestBody EchartsConfig echartsConfig){
		GsonOption gsonOption = EchartsCore.getInstance().adapterPie(echartsConfig,dataService.getPieData());
		//自定义配置
		gsonOption.title().x(X.center);
		gsonOption.legend().left(X.left).orient(Orient.vertical);
		return gsonOption.toString();
	}
	
	/**
	 * 双数值折线图
	 * @param echartsConfig
	 * @return
	 */
	@PostMapping(value = "doubleNumLine",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody String doubleNumLine(@RequestBody EchartsConfig echartsConfig){
		return EchartsCore.getInstance().adapterDoubleNumLine(echartsConfig,dataService.getDoubleNumLineData()).toString();
	}
	
	/**
	 * 双对象折线图
	 * @param echartsConfig
	 * @return
	 */
	@PostMapping(value = "doubleObjLine",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody String doubleObjLine(@RequestBody EchartsConfig echartsConfig){
		GsonOption gsonOption = EchartsCore.getInstance().adapterDoubleNumLine(echartsConfig,dataService.getObjectNumLineData());
		gsonOption.xAxis().get(0).type(AxisType.time);
		return gsonOption.toString();
	}
	
	
	/**
	 * 反转条形图
	 * @param echartsConfig
	 * @return
	 */
	@PostMapping(value = "reverseBar",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody String reverseBar(@RequestBody EchartsConfig echartsConfig){
		return EchartsCore.getInstance().adapterReverseBar(echartsConfig,dataService.getWeatherData()).toString();
	}
	
	/**
	 * 雷达图
	 * @param echartsConfig
	 * @return
	 */
	@PostMapping(value = "radar",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody String radar(@RequestBody EchartsConfig echartsConfig){
		return EchartsCore.getInstance().adapterRadar(echartsConfig,dataService.getRadarData()).toString();
	}
	
	
	/**
	 * 地图图表
	 * @param echartsConfig
	 * @return
	 */
	@PostMapping(value = "map",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody String map(@RequestBody EchartsConfig echartsConfig){
		return EchartsCore.getInstance().adapterMap(echartsConfig,getMapData()).toString();
	}
	
	/**测试数据请忽略
	 * 
	 * @return
	 */
	public Map<String,Map<String,Double>> getMapData(){
		Map<String,Map<String,Double>> map = new HashMap<>(16);
		
		Map<String,Double> iphone3 = new HashMap<>(16);
		iphone3.put("北京",randomData());
        iphone3.put("天津",randomData());
        iphone3.put("上海",randomData());
        iphone3.put("重庆",randomData());
        iphone3.put("河北",randomData());
        iphone3.put("河南",randomData());
        iphone3.put("云南",randomData());
        iphone3.put("辽宁",randomData());
        iphone3.put("黑龙江",randomData());
        iphone3.put("湖南",randomData());
        iphone3.put("安徽",randomData());
        iphone3.put("山东",randomData());
        iphone3.put("新疆",randomData());
        iphone3.put("江苏",randomData());
        iphone3.put("浙江",randomData());
        iphone3.put("江西",randomData());
        iphone3.put("湖北",randomData());
        iphone3.put("广西",randomData());
        iphone3.put("甘肃",randomData());
        iphone3.put("山西",randomData());
        iphone3.put("内蒙古",randomData());
        iphone3.put("陕西",randomData());
        iphone3.put("吉林",randomData());
        iphone3.put("福建",randomData());
        iphone3.put("贵州",randomData());
        iphone3.put("广东",randomData());
        iphone3.put("青海",randomData());
        iphone3.put("西藏",randomData());
        iphone3.put("四川",randomData());
        iphone3.put("宁夏",randomData());
        iphone3.put("海南",randomData());
        iphone3.put("台湾",randomData());
        iphone3.put("香港",randomData());
        iphone3.put("澳门",randomData());
		
		Map<String,Double> iphone4 = new HashMap<>(16);
		 iphone4.put("北京",randomData());
         iphone4.put("天津",randomData());
         iphone4.put("上海",randomData());
         iphone4.put("重庆",randomData());
         iphone4.put("河北",randomData());
         iphone4.put("安徽",randomData());
         iphone4.put("新疆",randomData());
         iphone4.put("浙江",randomData());
         iphone4.put("江西",randomData());
         iphone4.put("山西",randomData());
         iphone4.put("内蒙古",randomData());
         iphone4.put("吉林",randomData());
         iphone4.put("福建",randomData());
         iphone4.put("广东",randomData());
         iphone4.put("西藏",randomData());
         iphone4.put("四川",randomData());
         iphone4.put("宁夏",randomData());
         iphone4.put("香港",randomData());
         iphone4.put("澳门",randomData());
		
		Map<String,Double> iphone5 = new HashMap<>(16);
		
		 iphone5.put("北京",randomData());
         iphone5.put("天津",randomData());
         iphone5.put("上海",randomData());
         iphone5.put("广东",randomData());
         iphone5.put("台湾",randomData());
         iphone5.put("香港",randomData());
         iphone5.put("澳门",randomData());
		
		map.put("iphone3", iphone3);
		map.put("iphone4", iphone4);
		map.put("iphone5", iphone5);
		return map;
	}
	
	/**
	 * 日历图表
	 * @param echartsConfig
	 * @return
	 */
	@PostMapping(value = "calendar",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody List<Map<String,Object>> calendar(@RequestBody EchartsConfig echartsConfig){
		return getCalendarData();
	}
	
	/**
	 * echartsDate  工作          睡觉        娱乐
	 * 2017-05-01   10    8     6
	 * 2017-05-02   8     10    6
	 * @return
	 */
	/**
	 * 测试数据请忽略
	 * @return
	 */
	public  List<Map<String,Object>> getCalendarData(){
		int maxDaysOfMonth = 31;
		List<Map<String,Object>> list = new ArrayList<>();
		for(int i=1;i<=maxDaysOfMonth;i++){
			String dateDay="";
			if(i<10){
				dateDay = "0"+i;
			}else{
				dateDay = ""+i;
			}
			Map<String,Object> map = new HashMap<>(16);
			map.put("echartsDate", "2017-05-"+dateDay);
			map.put("工作", randomData()/100);
			map.put("睡觉", randomData()/100);
			map.put("娱乐", randomData()/100);
			list.add(map);
		}
		return list;
	}
	
	/**
	 * 热力图表
	 * @param echartsConfig
	 * @return
	 */
	@PostMapping(value = "scatterData",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody
	EchartsHeatMap scatterData(@RequestBody EchartsConfig echartsConfig){
		EchartsHeatMap echartsHeatMap = new EchartsHeatMap(dataService.getHeatMap());
		return echartsHeatMap;
		
	}
	
	
	private double randomData() {
	    return Math.round(Math.random()*1000);
	}
	
}
