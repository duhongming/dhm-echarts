package com.dhm;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;
/**
    @Author duhongming
    @Email 935720334@qq.com
    @Date 2018/5/9 20:14
*/
@SpringBootApplication
@EnableScheduling
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class EchartsApplication {
	public static void main(String[] args) {
        new SpringApplicationBuilder(
                EchartsApplication.class)
                .web(true).run(args);
    }  
}
