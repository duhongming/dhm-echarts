package com.dhm.aop;


import com.dhm.bean.EchartsConfig;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class EchartsLogsAspect {

    Logger logger = LoggerFactory.getLogger(EchartsLogsAspect.class);

    @Pointcut("execution(String com.dhm.web.*.*(com.dhm.bean.EchartsConfig))")
    public void getEchartsConfig(){}

    @Before("getEchartsConfig() && args(echartsConfig)")
    public void getEchartsConfigBefore(EchartsConfig echartsConfig){
        logger.info("传入配置参数：");
        logger.info(echartsConfig.toString());
        logger.info("传入表单参数：");
        logger.info(echartsConfig.getParameters().toString());
    }

    @AfterReturning(value = "getEchartsConfig()", returning="ret")
    public void doAfter(String ret){
        logger.info("图表Json格式数据：");
        logger.info(ret);
    }
//
//    @Around("getEchartsConfig()")
//    public void doAround(ProceedingJoinPoint pjp){
//        try {
//            pjp.proceed();
//
//            String methodName = pjp.getSignature().getName();
//            Object[] args = pjp.getArgs();
//            System.out.println("Arround:The method "+methodName +" begins with "+ new JsonUtil().toStrings(args));
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//        }
//    }
}
