package com.dhm.scheduled;

import com.alibaba.fastjson.JSON;
import com.dhm.config.RestTemplateConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
/**
 * 
 * @author duhongming
 *
 */
@Component
@Import(value = {RestTemplateConfig.class})
public class DataScheduledTask{
	
	@Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

	/**
	 * 折线图、条形图推送
	 * 每1秒执行一次
	 */
	@Scheduled(cron="0/1 * *  * * ? ")
	public void getLineAndBarDataScheduledTask(){
		Random random = new Random();
		Map<String,String> map = new HashMap<String,String>(4);
		map.put("最低气温",String.valueOf(random.nextInt(10)));
		map.put("最高气温",String.valueOf(random.nextInt(10)));
		map.put("xxx",LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		simpMessagingTemplate.convertAndSend("/topic/line/data", JSON.toJSONString(map));
		simpMessagingTemplate.convertAndSend("/topic/bar/data",JSON.toJSONString(map));
		//System.out.println("我是DataScheduledTask,getLineDataScheduledTask()正在发送后台推送数据："+new JsonUtil().toStrings(map));
	}

	/**
	 * 仪表盘和注水图推送
	 * 每1秒执行一次
	 */
	@Scheduled(cron="0/1 * *  * * ? ")
	public void getGaugeAndLiquidFillDataScheduledTask(){
		Random random = new Random();
		Double d = random.nextDouble()*100;
		DecimalFormat df = new DecimalFormat("######0.00");
		simpMessagingTemplate.convertAndSend("/topic/gauge/data", df.format(d));
		simpMessagingTemplate.convertAndSend("/topic/liquidfill/data", df.format(d));
		//System.out.println("我是DataScheduledTask,getGaugeDataScheduledTask()正在发送后台推送数据："+df.format(d));
	}

}
